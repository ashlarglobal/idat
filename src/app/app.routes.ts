import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';

//Authentication Routes
import { SignInComponent } from './components/authentication/sign-in/sign-in.component';
import { SignUpComponent } from './components/authentication/sign-up/sign-up.component';
import { ForgotPasswordComponent } from './components/authentication/forgot-password/forgot-password.component';
import { NewPasswordComponent } from './components/authentication/new-password/new-password.component';
//User Routes
import { MainComponent } from './components/user/main/main.component';
import { MyTingsComponent } from './components/user/my-tings/my-tings.component';
import { TingsHomeComponent } from './components/user/my-tings/tings-home/tings-home.component';
import { GalleryTingsComponent } from './components/user/my-tings/gallery-tings/gallery-tings.component';
import { SearchTingsComponent } from './components/user/my-tings/search-tings/search-tings.component';
import { TingDetailComponent } from './components/user/my-tings/ting-detail/ting-detail.component';
import { MyGroupsComponent } from './components/user/my-groups/my-groups.component';
import { GroupsHomeComponent } from './components/user/my-groups/groups-home/groups-home.component';
import { GroupDetailComponent } from './components/user/my-groups/group-detail/group-detail.component';
import { MySettingsComponent } from './components/user/my-settings/my-settings.component';
import { SettingsHomeComponent } from './components/user/my-settings/settings-home/settings-home.component';
import { UpdatePasswordComponent } from './components/user/my-settings/update-password/update-password.component';
import { MyFieldsComponent } from './components/user/my-fields/my-fields.component';
//Admin Routes
import { AdminHomeComponent } from './components/admin/admin-home/admin-home.component';
import { DashboardComponent } from './components/admin/dashboard/dashboard.component';
import { AdminTingsComponent } from './components/admin/admin-tings/admin-tings.component';
import { AdminTagsComponent } from './components/admin/admin-tags/admin-tags.component';
import { AdminAccountsComponent } from './components/admin/admin-accounts/admin-accounts.component';
import { AdminOrgsComponent } from './components/admin/admin-orgs/admin-orgs.component';
import { DashboardHomeComponent } from './components/admin/dashboard-home/dashboard-home.component';
import { QrScannerComponent } from './components/qr-scanner/qr-scanner.component';


export const routes: Routes = [
    {
      path: '',
      redirectTo: 'sign-in',
      pathMatch: 'full'
    },
    {
      path: 'home',
      component: MainComponent,
      children: [
        {
          path: '',
          redirectTo: 'my-tings',
          pathMatch: 'full'
        },
        {
          path: 'my-tings',
          component: MyTingsComponent,
          children: [
            {
              path: '',
              component: TingsHomeComponent
            },
            {
              path: 'gallery-tings',
              component: GalleryTingsComponent,
            },
            {
              path: 'search-tings',
              component: SearchTingsComponent
            }
            ,
            {
              path: 'ting-detail',
              component: TingDetailComponent
            }
          ]
        },
        {
          path: 'my-groups',
          component: MyGroupsComponent,
          children: [
            {
              path: '',
              redirectTo: 'groups-home',
              pathMatch: 'full'
            },
            {
              path: 'groups-home',
              component: GroupsHomeComponent
            },
            {
              path: 'group-detail',
              component: GroupDetailComponent
            }
          ]
        },
        {
          path: 'my-settings',
          component: MySettingsComponent,
          children: [
            {
              path: '',
              redirectTo: 'settings-home',
              pathMatch: 'full'
            },
            {
              path: 'settings-home',
              component: SettingsHomeComponent
            },
            {
              path: 'update-password',
              component: UpdatePasswordComponent
            }
          ]
        },
        {
          path: 'my-fields',
          component: MyFieldsComponent
        },
      ]
    },
    {
      path: 'admin',
      redirectTo: 'dashboard',
      pathMatch: 'full'
    },
    {
      path: 'dashboard',
      component: AdminHomeComponent,
      children: [
        {
          path: '',
          component: DashboardHomeComponent
        },
        {
          path: 'tings',
          component: AdminTingsComponent
        },
        {
          path: 'tags',
          component: AdminTagsComponent
        },
        {
          path: 'accounts',
          component: AdminAccountsComponent
        },
        {
          path: 'orgs',
          component: AdminOrgsComponent
        }
      ]
    },
    {
      path: 'sign-in',
      component: SignInComponent
    },
    {
      path: 'sign-up',
      component: SignUpComponent
    },
    {
      path: 'forget-password',
      component: ForgotPasswordComponent
    },
    {
      path: 'new-password',
      component: NewPasswordComponent
    },
    {
      path: 'qr-scanner',
      component: QrScannerComponent
    }
    
]
export const AppRoutes: ModuleWithProviders = RouterModule.forRoot(routes);
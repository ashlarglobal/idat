import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { config } from '../../common/config';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class AuthService {

  constructor(private http: HttpClient) { }

  signUp(user: any): Observable<any>{
    return this.http.post(`${config.apiUrl}/register`, user);
  }

}

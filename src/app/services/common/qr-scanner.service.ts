import { Injectable } from '@angular/core';

@Injectable()

export class QrScannerService {

  qrCode: string;
  decryptedLink: string = '';

  constructor() { }

  setQrCode(code: string) {
    this.qrCode = code;
  }

  getQrCode() {
    return this.qrCode;
  }

  setdecryptedLink(code: string) {
    this.qrCode = code;
  }

  getdecryptedLink() {
    return this.qrCode;
  }

}

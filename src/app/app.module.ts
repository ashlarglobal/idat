import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CdkTableModule } from '@angular/cdk/table';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
// import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


import { AppComponent } from './app.component';
import { SignInComponent } from './components/authentication/sign-in/sign-in.component';
import { SignUpComponent } from './components/authentication/sign-up/sign-up.component';
import { ForgotPasswordComponent } from './components/authentication/forgot-password/forgot-password.component';
import { NewPasswordComponent } from './components/authentication/new-password/new-password.component';

//User Routes
import { HomeComponent } from './components/user/home/home.component';
import { HeaderComponent } from './components/user/header/header.component';
import { MyTingsComponent } from './components/user/my-tings/my-tings.component';
import { MyGroupsComponent } from './components/user/my-groups/my-groups.component';
import { MySettingsComponent } from './components/user/my-settings/my-settings.component';
import { MyFieldsComponent } from './components/user/my-fields/my-fields.component';
import { SideNavComponent } from './components/user/side-nav/side-nav.component';
import { MainComponent } from './components/user/main/main.component';
import { GalleryTingsComponent } from './components/user/my-tings/gallery-tings/gallery-tings.component';
import { TingsHomeComponent } from './components/user/my-tings/tings-home/tings-home.component';
import { SearchTingsComponent } from './components/user/my-tings/search-tings/search-tings.component';
import { TingDetailComponent } from './components/user/my-tings/ting-detail/ting-detail.component';
import { GroupsHomeComponent } from './components/user/my-groups/groups-home/groups-home.component';
import { GroupDetailComponent } from './components/user/my-groups/group-detail/group-detail.component';
import { SettingsHomeComponent } from './components/user/my-settings/settings-home/settings-home.component';
import { UpdatePasswordComponent } from './components/user/my-settings/update-password/update-password.component';
import { BreadcrumbComponent } from './components/user/breadcrumb/breadcrumb.component';

//Admin Routes
import { DashboardComponent } from './components/admin/dashboard/dashboard.component';
import { AdminHomeComponent } from './components/admin/admin-home/admin-home.component';
import { AdminSideNavComponent } from './components/admin/admin-side-nav/admin-side-nav.component';
import { AdminTingsComponent } from './components/admin/admin-tings/admin-tings.component';
import { AdminTagsComponent } from './components/admin/admin-tags/admin-tags.component';
import { AdminAccountsComponent } from './components/admin/admin-accounts/admin-accounts.component';
import { AdminOrgsComponent } from './components/admin/admin-orgs/admin-orgs.component';
import { DashboardHomeComponent } from './components/admin/dashboard-home/dashboard-home.component';
import { QrScannerComponent } from './components/qr-scanner/qr-scanner.component';

import { AuthService } from './services/user/auth.service';
import { BreadcrumbService } from './services/user/breadcrumb.service';
import { GroupService } from './services/user/group.service';
import { QrScannerService } from './services/common/qr-scanner.service';

import { AppRoutes } from './app.routes';

//SERVICE WORKER
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';

import {
  // MatAutocompleteModule,
  // MatBadgeModule,
  // MatBottomSheetModule,
  // MatButtonModule,
  // MatButtonToggleModule,
  // MatCardModule,
  MatCheckboxModule,
  // MatChipsModule,
  // MatDatepickerModule,
  // MatDialogModule,
  // MatDividerModule,
  // MatExpansionModule,
  // MatGridListModule,
  // MatIconModule,
  // MatInputModule,
  // MatListModule,
  // MatMenuModule,
  // MatNativeDateModule,
  // MatPaginatorModule,
  // MatProgressBarModule,
  // MatProgressSpinnerModule,
  // MatRadioModule,
  // MatRippleModule,
  // MatSelectModule,
  // MatSidenavModule,
  // MatSliderModule,
  // MatSlideToggleModule,
  MatSnackBarModule,
  // MatSortModule,
  // MatStepperModule,
  // MatTableModule,
  // MatTabsModule,
  // MatToolbarModule,
  // MatTooltipModule,
  // MatTreeModule,
} from '@angular/material';


@NgModule({
  exports: [
    // CdkTableModule,
    // MatAutocompleteModule,
    // MatBadgeModule,
    // MatBottomSheetModule,
    // MatButtonModule,
    // MatButtonToggleModule,
    // MatCardModule,
    MatCheckboxModule,
    // MatChipsModule,
    // MatStepperModule,
    // MatDatepickerModule,
    // MatDialogModule,
    // MatDividerModule,
    // MatExpansionModule,
    // MatGridListModule,
    // MatIconModule,
    // MatInputModule,
    // MatListModule,
    // MatMenuModule,
    // MatNativeDateModule,
    // MatPaginatorModule,
    // MatProgressBarModule,
    // MatProgressSpinnerModule,
    // MatRadioModule,
    // MatRippleModule,
    // MatSelectModule,
    // MatSidenavModule,
    // MatSliderModule,
    // MatSlideToggleModule,
    MatSnackBarModule,
    // MatSortModule,
    // MatTableModule,
    // MatTabsModule,
    // MatToolbarModule,
    // MatTooltipModule,
    // MatTreeModule,
  ]
})
export class DemoMaterialModule {}

@NgModule({
  declarations: [
    AppComponent,
    AppComponent,
    SignInComponent,
    SignUpComponent,
    ForgotPasswordComponent,
    NewPasswordComponent,
    HomeComponent,
    HeaderComponent,
    MyTingsComponent,
    MyGroupsComponent,
    MySettingsComponent,
    MyFieldsComponent,
    SideNavComponent,
    MainComponent,
    GalleryTingsComponent,
    TingsHomeComponent,
    SearchTingsComponent,
    TingDetailComponent,
    GroupsHomeComponent,
    GroupDetailComponent,
    SettingsHomeComponent,
    UpdatePasswordComponent,
    DashboardComponent,
    AdminHomeComponent,
    AdminSideNavComponent,
    AdminTingsComponent,
    AdminTagsComponent,
    AdminAccountsComponent,
    AdminOrgsComponent,
    DashboardHomeComponent,
    BreadcrumbComponent,
    QrScannerComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    DemoMaterialModule,
    ReactiveFormsModule,
    AppRoutes,
    environment.production ? ServiceWorkerModule.register('ngsw-worker.js') : []
  ],
  providers: [AuthService, BreadcrumbService, GroupService, QrScannerService],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

declare var jquery:any;
declare var $ :any;

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})

export class HeaderComponent implements OnInit {

  activeClass: string;
  sidenavClass: Boolean = false;

  constructor(private router: Router) { }

  ngOnInit() {
    this.activeClass = 'tings';
    this.sidenavClass = true;
    
    $(".dropdown-trigger").dropdown();
    setTimeout(() => {
      $('.sidenav').sidenav();
    }, 500); 
  }

  onDestroy() {
    this.sidenavClass = false;
  }
  
  toggleDropdown() {
    $(".dropdown-trigger").dropdown();
  }

  onLogout() {
    this.router.navigate(['/sign-in']);
  }

}

import { Component, OnInit, ViewChild } from '@angular/core';

declare var jquery:any;
declare var $ :any;


@Component({
  selector: 'app-my-tings',
  templateUrl: './my-tings.component.html',
  styleUrls: ['./my-tings.component.css']
})

export class MyTingsComponent implements OnInit {

  @ViewChild('myModal') myModal;

  constructor() { }

  ngOnInit() {
  }

  openNewTingModal() {
    this.myModal.nativeElement.className = 'modal fade show';
  }

}

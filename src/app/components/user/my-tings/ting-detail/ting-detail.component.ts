import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';

declare var jquery:any;
declare var $ :any;

@Component({
  selector: 'app-ting-detail',
  templateUrl: './ting-detail.component.html',
  styleUrls: ['./ting-detail.component.css']
})
export class TingDetailComponent implements OnInit {

  toggleTagList: boolean = true;
  items: any[] = [
    {
      img: '../../../../assets/images/add-image1.png',
      name: 'image1.jpg',
    },
    {
      img: '../../../../assets/images/add-image1.png',
      name: 'image2.jpg',
    },
    {
      img: '../../../../assets/images/add-image1.png',
      name: 'image3.jpg',
    }
  ]

  constructor(
    private snackBar: MatSnackBar
  ) { }

  ngOnInit() {

    $('.modal').modal();
    
  }

  onHide() {
    this.toggleTagList = false;
  }

  onShow() {
    this.toggleTagList = true;
  }

  onClose(index) {
    this.items.splice(index, 1);
  }

  onDelete() {
    this.openSnackBar("Ting Deleted", '');
  }

  onDownload() {
    this.openSnackBar("Downloading...", '');
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, '', {
      duration: 2000,
    });
  }

}

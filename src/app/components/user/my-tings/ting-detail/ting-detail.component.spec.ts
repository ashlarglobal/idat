import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TingDetailComponent } from './ting-detail.component';

describe('TingDetailComponent', () => {
  let component: TingDetailComponent;
  let fixture: ComponentFixture<TingDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TingDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TingDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

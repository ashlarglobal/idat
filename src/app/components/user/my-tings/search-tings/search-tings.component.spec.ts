import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchTingsComponent } from './search-tings.component';

describe('SearchTingsComponent', () => {
  let component: SearchTingsComponent;
  let fixture: ComponentFixture<SearchTingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchTingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchTingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-search-tings',
  templateUrl: './search-tings.component.html',
  styleUrls: ['./search-tings.component.css']
})
export class SearchTingsComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  onTingDetail() {
    this.router.navigate(['/home/my-tings/ting-detail']);
  }

}

import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { QrScannerService } from '../../../../services/common/qr-scanner.service';
import { GalleryImage } from '../../../../models/GalleryImage';


declare var jquery:any;
declare var $ :any;
declare var qrcode: any;
declare var BrowserDetect: any;

@Component({
  selector: 'app-tings-home',
  templateUrl: './tings-home.component.html',
  styleUrls: ['./tings-home.component.css']
})
export class TingsHomeComponent implements OnInit {

  @ViewChild('winOpen') windOpen: ElementRef;

  tabState: number = 1;
  previewSrc: any;
  galleryImage: GalleryImage;
  sources: GalleryImage[] = [];
  decryptedLink: string = '';

  scanner: any = null;
  activeCameraId: any = null;
  cameras: any[] = [];
  scans: any[] = [];


  constructor(
    private router: Router,
    private snackBar: MatSnackBar,
    private qrScannerService: QrScannerService
  ) { }

  ngOnInit() {
      $('#new').modal();
      $('#move-to').modal();
      $('.tabs').tabs();

    this.reopenModalIfCodeExist();

    this.setQrCode();

  }

  setQrCode() {

    if(this.qrScannerService.getdecryptedLink) {
      this.decryptedLink = this.qrScannerService.getdecryptedLink();
    }
    
  }

  onGallery() {
    this.router.navigate(['/home/my-tings/gallery-tings']);
  }

  onTabSwitch() {
    if(this.tabState < 4) {
      this.tabState ++;
    }
  }

  onPrevTab() {
    if(this.tabState > 1) {
      this.tabState --;
    }
  }

  onSave() {
    this.openSnackBar("Ting Saved Successfully", '');
    $('#new').modal('close');
    setTimeout(() => {
      $('#move-to').modal('open');
    }, 1000);
  }

  reopenModalIfCodeExist() {
    let code = this.qrScannerService.getQrCode();
    if(code) {
      $('#new').modal('open');
    }
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, '', {
      duration: 2000,
    });
  }

  readURL(e) {

    if (e.currentTarget.files && e.currentTarget.files[0]) {

      let srcObj = {
        src: '',
        name: 'image'
      }
      
      const reader = new FileReader();
  
      reader.onload = (e) => {
        let target: any = e.target;
        let content: string = target.result;
        srcObj.src = content;
      }

      this.galleryImage = srcObj;

      this.sources.push(this.galleryImage);
  
      reader.readAsDataURL(e.currentTarget.files[0]);
    }
  }

  onRemoveSource(index) {
    this.sources.splice(index, 1);
  }

  onImageScan(e) { debugger

    const reader = new FileReader();

    reader.onload = () => {

      qrcode.callback = (res) =>  {

        if(res instanceof Error) {

          alert("No QR code found. Please make sure you selected the correct image form gallery and try again.");
        
        } else {

          this.qrScannerService.setQrCode = (res) ? res : '';

          this.decryptedLink = (res) ? res : '';
          console.log(res);

          // const elem = document.getElementById("linkid");
          // if (typeof elem.onclick == "function") {

          //     elem.onclick = () => {
          //       window.open(res, '_blank');
          //     }

          //     elem.onclick.apply(elem);
          // }


          const dialog = confirm("Your are redirecting to: " +  res);

          if(dialog) {

            let win = window.open(res, '_blank');
            win.focus();

          } else {

            return false;

          }

        }

      };
      qrcode.decode(reader.result);
    };

    reader.readAsDataURL(e.currentTarget.files[0]);

    }

    initIOSCam() {

      // if (BrowserDetect.browser === 'Safari' && BrowserDetect.version < 11 ) {

      //   return true;

      // } else if (window.navigator.userAgent.indexOf("Mac") != -1 && BrowserDetect.browser !== 'Safari') {

      //   return true;

      // }

      return false;

    }

}

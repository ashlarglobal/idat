import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TingsHomeComponent } from './tings-home.component';

describe('TingsHomeComponent', () => {
  let component: TingsHomeComponent;
  let fixture: ComponentFixture<TingsHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TingsHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TingsHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

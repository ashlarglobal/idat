import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

declare var jquery:any;
declare var $ :any;


@Component({
  selector: 'app-gallery-tings',
  templateUrl: './gallery-tings.component.html',
  styleUrls: ['./gallery-tings.component.css']
})

export class GalleryTingsComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  onSearchTings() {
    this.router.navigate(['/home/my-tings/search-tings']);
  }

  onTingDetail() {
    this.router.navigate(['/home/my-tings/ting-detail']);
  }

  onSelectAll() {
    $('#select-all').click(function() {
      $('input[name=checkall]').prop('checked', true);
    });
  }

  onClear() {
    $('#clear-all').click(function() {
      $('input[name=checkall]').prop('checked', false);
    });  
  }

}

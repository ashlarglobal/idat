import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GalleryTingsComponent } from './gallery-tings.component';

describe('GalleryTingsComponent', () => {
  let component: GalleryTingsComponent;
  let fixture: ComponentFixture<GalleryTingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GalleryTingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GalleryTingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

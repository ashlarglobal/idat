import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyTingsComponent } from './my-tings.component';

describe('MyTingsComponent', () => {
  let component: MyTingsComponent;
  let fixture: ComponentFixture<MyTingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyTingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyTingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BreadcrumbService } from '../../../../services/user/breadcrumb.service';
import { GroupService } from '../../../../services/user/group.service';

declare var jquery:any;
declare var $ :any;


@Component({
  selector: 'app-group-detail',
  templateUrl: './group-detail.component.html',
  styleUrls: ['./group-detail.component.css']
})
export class GroupDetailComponent implements OnInit {

  subGroups: any[];

  constructor(
    private router: Router,
    public breadcrumbService: BreadcrumbService,
    public groupService: GroupService
  ) { }

  ngOnInit() {
    $('.modal').modal();
    this.subGroups = this.groupService.subGroups;
  }

  onTingDetail() {
    this.router.navigate(['/home/my-tings/ting-detail']);
  }

  onGroupDetail(crumb: string) {
    // this.breadcrumbService.breadcrumb = `My Groups > ${crumb}`;
    this.router.navigate(['/home/my-groups/group-detail']);
  }

  onRightClick(e) {
    // $(".my-group-box.personal").bind('contextmenu', function (e) {

      let top = e.pageY+5;
      let left = e.pageX;

      // Show contextmenu
      $(".my-group-hide-box-personal").toggle(500).css({
        top: top + "px",
        left: left + "px"
      });

      // disable default context menu
      return false;
    // });
  }

}

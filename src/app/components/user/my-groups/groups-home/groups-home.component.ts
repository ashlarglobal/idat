import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BreadcrumbService } from '../../../../services/user/breadcrumb.service';
import { GroupService } from '../../../../services/user/group.service';

declare var jquery:any;
declare var $ :any;

@Component({
  selector: 'app-groups-home',
  templateUrl: './groups-home.component.html',
  styleUrls: ['./groups-home.component.css']
})

export class GroupsHomeComponent implements OnInit {

  items: any[] = [
    {
      label: 'Rename'
    },
    {
      label: 'Make Default'
    },
    {
      label: 'Delete'
    }
  ]

  groups: any[] = [
    {
      group: 'Personal Tings',
      subgroups: [
        'HMS Bounty',
        'QE2'
      ]
    },
    {
      group: 'Fleet A',
      subgroups: [ 
        'Fleet B',
        'Fleet C'
      ]
    }
  ];

  constructor(
    private router: Router,
    public breadcrumbSercice: BreadcrumbService,
    public groupService: GroupService
  ) { }

  ngOnInit() {


    $('.modal').modal();
  
    
    this.breadcrumbSercice.breadcrumb = [];
    this.breadcrumbSercice.breadcrumb.push('My Groups');
  }

  onGroupDetail(group: any) {

    this.groupService.subGroups = group.subgroups;
    this.breadcrumbSercice.breadcrumb.push(group.group);
    this.router.navigate(['/home/my-groups/group-detail']);

  }

  onRightClick(e) {
    // $(".my-group-box.personal").bind('contextmenu', function (e) {

      let top = e.pageY+5;
      let left = e.pageX;

      // Show contextmenu
      $(".my-group-hide-box-personal").toggle(500).css({
        top: top + "px",
        left: left + "px"
      });

      // disable default context menu
      return false;
    // });
  }
  

}

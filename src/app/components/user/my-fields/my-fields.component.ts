import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-my-fields',
  templateUrl: './my-fields.component.html',
  styleUrls: ['./my-fields.component.css']
})
export class MyFieldsComponent implements OnInit {

  newField: string;
  removeIndex: number;

  fields: string[] = [
    'Make',
    'Model',
    'Type',
    'Date of Birth',
    'Description',
    'Language',
    'Serial',
    'Make',
    'Model',
    'Type',
    'Date of Birth',
    'Description',
    'Language',
    'Serial',
  ];

  constructor() { }

  ngOnInit() {
  }

  onAdd() {
    if(!this.newField) {
      alert('Empty Field');
    }
    this.fields.unshift(this.newField);
    this.newField = '';
  }

  onOptions(index) {
    this.removeIndex = index;
  }

  onDelete() {
    this.fields.splice(this.removeIndex, 1);
  }

}

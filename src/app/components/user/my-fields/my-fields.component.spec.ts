import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyFieldsComponent } from './my-fields.component';

describe('MyFieldsComponent', () => {
  let component: MyFieldsComponent;
  let fixture: ComponentFixture<MyFieldsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyFieldsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyFieldsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { BreadcrumbService } from '../../../services/user/breadcrumb.service';

@Component({
  selector: 'app-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.css']
})
export class BreadcrumbComponent implements OnInit {

  breadcrumb: string = '';

  constructor(public breadcrumbService: BreadcrumbService) { }

  ngOnInit() { 
    let crumb: any[];
    crumb = this.breadcrumbService.breadcrumb; console.log(this.breadcrumbService.breadcrumb)
    this.breadcrumb = crumb.join(' > '); 
  }

}

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-update-password',
  templateUrl: './update-password.component.html',
  styleUrls: ['./update-password.component.css']
})
export class UpdatePasswordComponent implements OnInit {

  constructor(
    private router: Router,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit() {
  }

  onSave() {
    this.openSnackBar("Password Updated", '');
    // this.router.navigate(['/home/my-settings']);
  }

  onCancel() {
    this.router.navigate(['/home/my-settings']);
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, '', {
      duration: 2000,
    });
  }

}

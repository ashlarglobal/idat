import { Component, VERSION, OnInit, ViewChild } from '@angular/core';
import { QrScannerService } from '../../services/common/qr-scanner.service';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';

declare var require: any;
declare var Instascan: any;

@Component({
  selector: 'app-qr-scanner',
  templateUrl: './qr-scanner.component.html',
  styleUrls: ['./qr-scanner.component.css']
})
export class QrScannerComponent implements OnInit {

  scanner: any = null;
  activeCameraId: any = null;
  cameras: any[] = [];
  scans: any[] = [];

  constructor(
    private qrScannerService: QrScannerService,
    private router: Router,
    private snackBar: MatSnackBar
  ) {

  }

  ngOnInit() {

    
    this.scanner = new Instascan.Scanner({ video: document.getElementById('preview'), scanPeriod: 5 });

    this.scanner.addListener('scan', (content, image) => {

      this.qrScannerService.setQrCode((content) ? content : '');
      
      const dialog = confirm("Your are redirecting to: " +  content);
      

      if(dialog) {

        let win =window.open(content, '_blank');
        win.focus();
        this.router.navigate(['../home/my-tings'])

      } else {

        return false;

      }
      
    });

    Instascan.Camera.getCameras().then( (cameras) => {
      this.cameras = cameras;
      console.log(this.cameras);

      if (this.cameras.length > 0) {

        if(navigator.userAgent.indexOf("Chrome") !== -1) {

          console.log('chrome')
          console.log(this.cameras)

          if(this.cameras[1] && this.cameras[1] !== undefined) {

            this.activeCameraId = this.cameras[1].id;
            this.scanner.start(this.cameras[1]);
  
          } else {
  
            this.activeCameraId = this.cameras[0].id;
            this.scanner.start(this.cameras[0]);
  
          }

        } else {

          console.log(this.cameras)
          console.log('safari');

          this.activeCameraId = this.cameras[0].id;
          this.scanner.start(this.cameras[0]);

        }

        

      } else {
        console.error('No cameras found.');
      }
    }).catch(function (e) {
      console.error(e);
    });

  }

  ngOnDestroy() {
    this.scanner.stop().then(() => {
      console.log('scanner stoped');
    })
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, '', {
      duration: 2000,
    });
  }

}

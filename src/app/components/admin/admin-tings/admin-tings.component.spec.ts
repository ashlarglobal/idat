import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminTingsComponent } from './admin-tings.component';

describe('AdminTingsComponent', () => {
  let component: AdminTingsComponent;
  let fixture: ComponentFixture<AdminTingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminTingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminTingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

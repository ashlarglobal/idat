import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-admin-side-nav',
  templateUrl: './admin-side-nav.component.html',
  styleUrls: ['./admin-side-nav.component.css']
})
export class AdminSideNavComponent implements OnInit {

  activeClass: string;
  sidenavClass: Boolean = false;

  constructor() { }

  ngOnInit() {
    this.activeClass = 'dashboard';
    this.sidenavClass = true;
  }

  onDestroy() {
    this.sidenavClass = false;
  }

}

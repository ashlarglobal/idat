import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';

declare var jquery:any;
declare var $ :any;


@Component({
  selector: 'app-admin-orgs',
  templateUrl: './admin-orgs.component.html',
  styleUrls: ['./admin-orgs.component.css']
})
export class AdminOrgsComponent implements OnInit {

  constructor(
    private snackBar: MatSnackBar
  ) { }

  ngOnInit() {

    $('.modal').modal();

  }

  onAddOrganization() {
    this.openSnackBar("Organization Added", '');
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, '', {
      duration: 2000,
    });
  }

}

import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../services/user/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {

  email: string;
  password: string;

  constructor
  (
    private authService: AuthService,
    private router: Router
  ) { }

  ngOnInit() {
  }

  onSignUp() {
    this.router.navigate(['home']);
    // this.authService.signUp({email: this.email, password: this.password})
    // .subscribe()
  }

}
